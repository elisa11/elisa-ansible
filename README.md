# Elisa Ansible

Set of Ansible scripts for automating code deployment, infrastructure and configuration settings required for developing and using Elisa backend and frontend applications. The scripts are intended to be run in an isolated environment (e.g. virtual machine).

**The entire process of Ansible deployment is thoroughly explained in the attachment part of 2021 backend documentation**. It also covers possible problems that may arise at each step so it is highly recommended to follow this process in order to avoid any issues. For details regarding the frontend deployment specifics, see the 2021 frontend documentation.

## Requirements
 - Ansible >=2.10
 - Fedora 35 Linux VM or other RHEL distribution  (only .rpms of Oracle instant client are available without registration)
## Important notes before running Ansible scripts
- before running the Ansible scripts (playbooks), in files **inventory.ini** and **/variables/devel.yml** certain values marked by # (or ###) need to be changed based on your own data (further explained in documentation user manual for deploying Elisa).
- scripts are constructed to be used with VM user **elisa** that has a password **secret** and has *sudo* privileges - this allows you to have backend database set up without any further changes needed. It is not recommended to run the scripts with a different user-password combination as it may require additional changes to database configurations and/or scripts. 
- additional sensitive information will be required to be added to file **/variables/devel.yml** before running the Ansible scripts. The supervisor is aware of this and will provide it on demand.
- in case of failure during execution of the Ansible playbooks, rerunning the already successfully applied playbook again *should* not cause any problems.
## Running the scripts/playbooks
- running specific playbook:
`ansible-playbook -i <path_to_inventory.ini> <path_to_playbook>`
- running playbook **/playbooks/install_devel.yml**  will run all existing playbooks in the Ansible project in the correct order and is recommended usage.


## Finalizing the backend application initialization
After successfully running the scripts, in order to allow the user to use practical Python virtual environment, the following steps will need to be run manually to finish initializing the application settings:
- create and activate virtual environment inside the Django application directory 
(*elisa-api/elisa-api-django*) that was cloned/pulled when running Ansible scripts
- install dependencies from *requirements.txt* using `pip install -r requirements.txt` inside the activated virtual environment
- run these 3 scripts (in this order):
`python manage.py migrate_schemas --shared`
`python manage.py init_app`
`python manage.py set_user_role <your_ais_username> <your_ais_id> 1` 
- before running the last script that will import initial data, create process for SSH tunnel connection using private key *elisa_priv.pem* (this file will be provided on demand by your supervisor): 
`sudo ssh -o StrictHostKeyChecking=no -o ServerAliveInterval=60 -i elisa_priv.pem -fNT -L 12345:db-new.is.stuba.sk:1521 ubuntu@147.175.121.233`
- finally, run `python manage.py initdbdata` to import initial data from database (users, departmends and periods) - if you get an error saying **Cannot locate 64-bit Oracle Client library"* when running the initdbdata script, run 
`export LD_LIBRARY_PATH=/usr/lib/oracle/21.1/client64/lib:$LD_LIBRARY_PATH` 
to create the required environment variable defining path to Oracle Client library and then rerun the initdbdata script. If an error *"TNS: no listener"* occurs when running the script, it means the SSH tunnel connection is not active.

> **Note:** it is not required to use virtual environment but it is a good practice. It also allows you to use different versions of Python/pip to run your code/download dependencies as required. If you decide not to use the virtual environment, the 3 python scripts will need to be run in global environment to finish initializing the application.

## Running the application
Before running the application, make sure you have successfully executed previous steps - if unsure, refer to the documentation

In order to run the **backend** Django application run command
`python manage.py runsslserver <VM_IP>:<PORT>` inside the Django directory, where *<VM_IP>* corresponds to the IP address of your virtual machine and *\<PORT\>* should match the configuration settings of frontend Spring profile being used (or vice versa) - more information about required frontend adjustments can be found in 2021 frontend documentation. After running the above command you will be able to access Django server at *https<k>://\<</k>VM_IP\>:\<PORT\>* either on your host or guest machine.

To run the **frontend** Angular application, it is required that the Spring Boot configuration server is set up correctly and running. You will need to edit Spring profile configuration files and use your own virtual machine IP address as mentioned in the frontend documentation and then rebuild the .jar file by executing `mvn package -B -e` inside */Prototyp-ui/elisa-ui-api* directory. To launch the Spring Boot server, run command 
`java -jar elisa-ui-api-endpoint/target/config-server.jar` inside the *Prototyp-ui/elisa-ui-api* directory. Before running the Angular application, you will need to download Angular cli package by running `sudo npm install -g @angular/cli@latest` (you might want to substitute @latest with your desired version)
Then you can build and launch the Angular application by running 
`ng serve –-proxy-config src/proxy.conf.js -–host 0.0.0.0` inside the Angular directory. If everything is done correctly, the frontend website will be accessible at  *http<k>://\<</k>VM_IP\>*. To be able to log in through the website, VPN connection has to be active and the backend application must be running.

### Notes
- to be able to reach university LDAP server (used for logging in application), you have to activate VPN. The Ansible scripts do create a VPN connection on VM, however you can also configure the VPN connection yourself on the host PC and it will also work just fine
- when working with VirtualBox, make sure to use the *Snapshot* functionality as to make sure you do not have to repeat the same process again in case something goes wrong
